package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag<Number> createBag(ArrayList<Number> values){
         return new IntegersBag<Number>(values);		
	}
	
	
	public static Number getMean(IntegersBag<Number> bag){
		return model.computeMean(bag);
	}
	
	public static Number getMax(IntegersBag<Number> bag){
		return model.getMax(bag);
	}
	public static Number getMin(IntegersBag<Number> bag){
		return model.getMin(bag);
	}
	public static Number getEvens(IntegersBag<Number> bag){
		return model.getEvens(bag);
	}
	//Aqu� llamamos al metodo del modelo, la gracia es que la vista no interactue directamente sobre
	//el para as�, tener un desarrollo ag�l y modular en el proyecto.
	public static Number getCousins(IntegersBag<Number> bag){
		return model.getCousins(bag);
	}
	
}
