package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class IntegersBag <T extends Number> {
	
	private HashSet<Number> bag;
	
	public IntegersBag(){
		this.bag = new HashSet<Number>();
	}
	
	public IntegersBag(ArrayList<Number> data){
		this();
		if(data != null){
			for (Number datum : data) {
				bag.add(datum);
			}
		}
		
	}
	
	
	public void addDatum(Number datum){
		bag.add(datum);
	}
	
	public Iterator<Number> getIterator(){
		return this.bag.iterator();
	}

}
