package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {



	public double computeMean(IntegersBag<Number> bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				mean =mean+ (Double) iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}


	public int getMax(IntegersBag<Number> bag){
		int max = Integer.MIN_VALUE;
		int value;
		if(bag != null){
			Iterator<Number> iter = bag.getIterator();
			while(iter.hasNext()){
				value = (Integer) iter.next();
				if( max < value){
					max = value;
				}
			}

		}
		return max;
	}
	public int getMin(IntegersBag<Number> bag) {
		int min =Integer.MAX_VALUE;
		int value;
		if (bag!=null) {
			Iterator<Number> iter =bag.getIterator();
			while (iter.hasNext()) {
				value=(Integer) iter.next();
				if (min>value) {
					min=value;
				}
			}
		}
		return min;
	}

	public int getEvens(IntegersBag<Number> bag)
	{
		int count=0;
		if (bag!=null) {
			Iterator<Number> iter = bag.getIterator();
			while (iter.hasNext()) {
				int toCompare=(Integer) iter.next();
				if (toCompare%2==0) {
					count++;
				}
			}
		}
		return count;
	}

	/**
	 * Devuelve la cantidad de numeros primos en el arreglo
	 * @param bag
	 * @return int >0;
	 */
	public int getCousins (IntegersBag<Number> bag)
	{
		int count = 0; //contador
		if (bag!=null) { // Condicional de que la bolsa no sea nula
			Iterator<Number> iter = bag.getIterator(); //Obtiene el iterator de la bolsa
			while (iter.hasNext()) 
			{
				int value=(Integer) iter.next();
				int a=0;
				for (int j = 1; j < (value+1); j++) 
				{
					if (value%j==0) 
					{
						a++;
					}
				}
				if (a==2) {
					count++;
				}
			}
		}
		return count; //retorna 
		//Al ser patron MVC- Model-View-Controller ahora saltamos a View
	}
}


